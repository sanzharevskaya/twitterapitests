package selenium.utils;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Instant;

public class SetupPages {
    protected static WebDriver driver;

    protected static final String LOGIN = "sanjarevskaya";
    protected static final String PASSWORD = "lammo131";

    protected String TWITTER_HOME = "http://twitter.com";
    protected String LOGIN_INPUT = "signin-email";
    protected String PASSWORD_INPUT = "signin-password";
    protected String AUTH_BUTTON = ".//*[1][@type='submit']";
    protected String ALL_TWITS = "//*[contains(@id, 'stream-item-tweet')]";
    protected String PATH_TO_CREATED_AT = "/div[1]/div[2]/div[1]/small/a/span[1]";
    protected String PATH_TO_RETWEET_COUNT = "/div[1]/div[2]/*[@class='stream-item-footer']/div[2]/div[2]/button[1]/span/span";
    protected String PATH_TO_TEXT = "/div[1]/div[2]/div[2]/p";
    protected String PATH_TO_DROPDOWN = "/div[1]/div[2]/div[1]/div/div/button";
    protected String NEW_TWITT = "global-new-tweet-button";
    protected String NEW_TWITT_INPUT = ".//*[@id='tweet-box-global']";
    protected String POST_NEW_TWITT_BUTTON = ".//*[@id='global-tweet-dialog-dialog']/div[2]/div[4]/form/div[3]/div[2]/button";
    protected String DELETE_THE_TWITT_BUTTON = ".//*[@class='dropdown open']/div/ul/*[@class='js-actionDelete']/button";
    protected String CONFIRM_THE_DELETING = ".//*[@id='delete-tweet-dialog-dialog']/div[2]/div[4]/button[2]";

    public DesiredCapabilities setUp() {
        DesiredCapabilities capabilities = DesiredCapabilities.firefox();
        capabilities.setCapability("marionette", true);
        System.setProperty("webdriver.gecko.driver", "//Users//Diana//Desktop//geckodriver");
        return capabilities;
    }

    protected void waitJQueryRequestsLoad() {
        WebDriverWait wait = new WebDriverWait(driver, 25);
        ExpectedCondition<Boolean> jQueryLoadCondition = new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                JavascriptExecutor js = (JavascriptExecutor) driver;
                return (Boolean) js.executeScript("return jQuery.active == 0");
            }
        };
        wait.until(jQueryLoadCondition);
    }

    protected long getTimestamp() {
        return Instant.now().getEpochSecond();
    }
}
