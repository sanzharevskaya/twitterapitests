package selenium.utils.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import selenium.utils.SetupPages;
import tests.models.status.UserStatus;

import java.util.ArrayList;
import java.util.List;

public class MainPage extends SetupPages {

    private UserStatus createUserStatusInstance() {
        return new UserStatus();
    }

    MainPage(WebDriver driver) {
        this.driver = driver;
    }

    public ArrayList<UserStatus> getListOfPopularTwitts() {
        ArrayList<UserStatus> statuses = new ArrayList<>();
        List<WebElement> list = driver.findElements(By.xpath(ALL_TWITS));
        for (int i = 1; i <= list.size(); i++) {
            UserStatus userStatus = createUserStatusInstance();
            userStatus.setCreatedAt(driver.findElement(By.xpath(ALL_TWITS + "[" + i + "]" + PATH_TO_CREATED_AT)).getText());
            if (!driver.findElement(By.xpath(ALL_TWITS + "[" + i + "]" + PATH_TO_RETWEET_COUNT)).getText().isEmpty()) {
                userStatus.setRetweetCount(Integer.parseInt(driver.findElement(By.xpath(ALL_TWITS + "[" + i + "]" + PATH_TO_RETWEET_COUNT)).getText()));
            }
            userStatus.setText(driver.findElement(By.xpath(ALL_TWITS + "[" + i + "]" + PATH_TO_TEXT)).getText());
            statuses.add(userStatus);
        }
        return statuses;
    }

    public NewStatusPage postNewStatus(){
        driver.findElement(By.id(NEW_TWITT)).click();
        return new NewStatusPage(driver);
    }

    public UserStatus getTheFirstStatus(){
        UserStatus status = createUserStatusInstance();
        status.setText(driver.findElement(By.xpath(ALL_TWITS + "[1]" + PATH_TO_TEXT)).getText());
        return status;
    }

    public UserStatus destroyTheStatus(){
        driver.findElement(By.xpath(ALL_TWITS + "[1]" + PATH_TO_DROPDOWN)).click();
        driver.findElement(By.xpath(DELETE_THE_TWITT_BUTTON)).click();
        driver.findElement(By.xpath(CONFIRM_THE_DELETING)).click();
        waitJQueryRequestsLoad();
        return getTheFirstStatus();
    }

}
