package selenium.utils.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import selenium.utils.SetupPages;

public class NewStatusPage extends SetupPages {

    public NewStatusPage(WebDriver driver) {
        this.driver = driver;
    }

    public MainPage sendTwittTextAndPostTheTwitt(String twittText){
        driver.findElement(By.xpath(NEW_TWITT_INPUT)).sendKeys(twittText);
        driver.findElement(By.xpath(POST_NEW_TWITT_BUTTON)).click();
        waitJQueryRequestsLoad();
        return new MainPage(driver);
    }

}
