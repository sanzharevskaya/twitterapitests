package selenium.utils.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import selenium.utils.SetupPages;

import java.util.concurrent.TimeUnit;

public class AuthorizationForm extends SetupPages{

    public AuthorizationForm(WebDriver driver) {
        this.driver = driver;
    }

    public MainPage authorize(String login, String password){
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id(LOGIN_INPUT)).sendKeys(login);
        driver.findElement(By.id(PASSWORD_INPUT)).sendKeys(password);
        driver.findElement(By.xpath(AUTH_BUTTON)).click();
        return new MainPage(driver);
    }
}
