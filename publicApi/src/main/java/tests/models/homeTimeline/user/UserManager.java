package tests.models.homeTimeline.user;

import twitter4j.Status;

public class UserManager {

    private User createUserInstance() {
        return new User();
    }

    public User setUserStructure(Status status) {
        User user = createUserInstance();
        user.setCreatedAt(status.getUser().getCreatedAt().toString());
        return user;
    }
}
