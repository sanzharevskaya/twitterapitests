package tests.models.status;

import lombok.Getter;
import lombok.Setter;
import tests.models.homeTimeline.user.User;

public class UserStatus {

    @Getter @Setter private String createdAt;
    @Getter @Setter private Integer retweetCount;
    @Getter @Setter private String text;
    @Getter @Setter private User user;
    @Getter @Setter private String statusId;
    @Getter @Setter private Long id;

    public void setUser(User user) {
        this.user = user;
    }

}
