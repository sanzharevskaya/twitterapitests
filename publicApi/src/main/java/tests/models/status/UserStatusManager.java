package tests.models.status;

import tests.models.TwitterClient;
import tests.models.homeTimeline.user.User;
import tests.models.homeTimeline.user.UserManager;
import twitter4j.Status;

import java.util.ArrayList;


public class UserStatusManager {
    private TwitterClient twitterClient = new TwitterClient();

    private UserStatus createUserStatusInstance() {
        return new UserStatus();
    }

    private UserManager userManager = new UserManager();

    public ArrayList<UserStatus> setHomeTimelinesStructure() {
        ArrayList<Status> homeTimelines = twitterClient.getHomeTimeLine();
        ArrayList<UserStatus> userStatuses = new ArrayList<>();

        homeTimelines.forEach(homeTimeline -> {
            UserStatus userStatus = createUserStatusInstance();
            userStatus.setCreatedAt(homeTimeline.getCreatedAt().toString());
            userStatus.setRetweetCount(homeTimeline.getRetweetCount());
            userStatus.setText(homeTimeline.getText());
            User gottenUser = userManager.setUserStructure(homeTimeline);
            userStatus.setUser(gottenUser);
            userStatuses.add(userStatus);
        });

        return userStatuses;
    }

    public UserStatus updateTheStatus(String text) {
        Status status = twitterClient.updateStatus(text);
        UserStatus userStatus = createUserStatusInstance();
        userStatus.setText(status.getText());
        userStatus.setId(status.getId());

        return userStatus;
    }

    public int updateTheStatusTwice(String text) {
        return twitterClient.getStatusCodeAfterDuplicatingOfStatus(text);
    }

    public UserStatus destroyTheStatusById(long id) {
        Status status = twitterClient.destroyStatus(id);
        UserStatus destroyedUserStatus = createUserStatusInstance();
        destroyedUserStatus.setText(status.getText());
        destroyedUserStatus.setId(status.getId());

        return destroyedUserStatus;
    }

    public int destroyTheStatusTwice(long id) {
        return twitterClient.getStatusCodeAfterTwiceDeleting(id);
    }
}
