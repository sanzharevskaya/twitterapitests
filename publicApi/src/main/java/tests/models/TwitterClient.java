package tests.models;

import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

import java.util.ArrayList;

public class TwitterClient {

    private String ACCESS_TOKEN = "2363944712-Wea5voW25M5D4TPoZQhZ670BkPrvOjOxHJ7NE6q";
    private String ACCESS_SECRET = "D3IfBSlYUuLRJfr3hHOW6ce966CKPdsZ6kfsyvKMGBvzB";
    private String CONSUMER_KEY = "PfTle3eKpksWTuzC0Rjhuko6G";
    private String CONSUMER_SECRET = "cjGrEWd5tMv4O3mdrgcyCLvaQkcry4byfGemaqdK0HUuAzcxPt";

    private Twitter twitter;

    public TwitterClient() {
        configure();
    }

    private void configure() {
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true)
                .setOAuthConsumerKey(CONSUMER_KEY)
                .setOAuthConsumerSecret(CONSUMER_SECRET)
                .setOAuthAccessToken(ACCESS_TOKEN)
                .setOAuthAccessTokenSecret(ACCESS_SECRET);
        TwitterFactory tf = new TwitterFactory(cb.build());

        twitter = tf.getInstance();
    }

    public ArrayList<Status> getHomeTimeLine() {
        ArrayList<Status> statuses = new ArrayList<>();
        try {
            twitter.timelines().getHomeTimeline().forEach(statuses::add);
        } catch (TwitterException e) {
            e.printStackTrace();
        }
        return statuses;
    }

    public Status updateStatus(String text) {
        Status status = null;
        try {
            status = twitter.updateStatus(text);
        } catch (TwitterException e) {
            e.printStackTrace();
        }
        return status;
    }

    public int getStatusCodeAfterDuplicatingOfStatus(String text) {
        int statusCode = 0;
        int i = 0;
        try {
            while (i < 2) {
                twitter.updateStatus(text);
                i++;
            }
        } catch (TwitterException e) {
            e.printStackTrace();
            statusCode = e.getStatusCode();
        }
        return statusCode;
    }

    public Status destroyStatus(Long id) {
        Status status = null;
        try {
            status = twitter.destroyStatus(id);
        } catch (TwitterException e) {
            e.printStackTrace();
        }
        return status;
    }

    public int getStatusCodeAfterTwiceDeleting(Long id) {
        int statusCode = 0;
        int i = 0;
        try {
            while (i < 2) {
                twitter.destroyStatus(id);
                i++;
            }
        } catch (TwitterException e) {
            e.printStackTrace();
            statusCode = e.getStatusCode();
        }
        return statusCode;
    }
}
