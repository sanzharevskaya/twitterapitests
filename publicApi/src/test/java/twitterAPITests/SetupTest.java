package twitterAPITests;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;

public class SetupTest {

    static boolean checkDatePattern(String date) {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy");
            formatter.parse(date);
            return true;
        } catch (ParseException e) {
            return false;
        }
    }

    long getTimestamp() {
        return Instant.now().getEpochSecond();
    }
}
