package twitterAPITests;

import org.testng.annotations.Test;
import tests.models.status.UserStatus;
import tests.models.status.UserStatusManager;

import java.util.ArrayList;

public class StatusTests extends Checker {

    private UserStatusManager userStatusManager = new UserStatusManager();

    @Test
    public void getTimeLineTest() {
        ArrayList<UserStatus> statuses = userStatusManager.setHomeTimelinesStructure();
        checkTimeLineResponse(statuses);
    }

    @Test
    public void updateTheStatusWithUniqueTextTest() {
        String textOfStatus = "It took less than " + getTimestamp() + " seconds.";
        UserStatus status = userStatusManager.updateTheStatus(textOfStatus);
        checkTheNewStatus(status, textOfStatus);
    }

    @Test
    public void updateTheStatusTwiceWithNotUniqueTextTest() {
        String textOfStatus = "It took less than " + getTimestamp() + " seconds.";
        int expectedStatusCode = 403;
        int actualStatusCode = userStatusManager.updateTheStatusTwice(textOfStatus);
        checkTheErrorResponseCode(actualStatusCode, expectedStatusCode);
    }

    @Test
    public void destroyTheStatusTest() {
        String textOfAddedStatus = getTimestamp() + " seconds is too long.";
        UserStatus status = userStatusManager.updateTheStatus(textOfAddedStatus);
        long newStatusId = status.getId();
        status = userStatusManager.destroyTheStatusById(newStatusId);
        checkTheDestroyedStatus(status, textOfAddedStatus, newStatusId);
    }

    @Test
    public void destroyTheStatusTwiceTest() {
        String textOfAddedStatus = getTimestamp() + " seconds is too long.";
        UserStatus status = userStatusManager.updateTheStatus(textOfAddedStatus);
        long newStatusId = status.getId();
        int expectedStatusCode = 404;
        int actualStatusCode = userStatusManager.destroyTheStatusTwice(newStatusId);
        checkTheErrorResponseCode(actualStatusCode, expectedStatusCode);
    }
}
