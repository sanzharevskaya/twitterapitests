package twitterAPITests;

import tests.models.status.UserStatus;

import java.util.ArrayList;

import static org.testng.Assert.*;

class Checker extends SetupTest {

    void checkTimeLineResponse(ArrayList<UserStatus> timelines) {
        timelines.forEach(timeline -> {
            assertNotNull(timeline.getCreatedAt());
            assertNotNull(timeline.getRetweetCount());
            assertNotNull(timeline.getText());
            assertNotNull(timeline.getUser().getCreatedAt());

            assertTrue(checkDatePattern(timeline.getCreatedAt()));
            assertTrue(timeline.getRetweetCount() >= 0);
            assertTrue(timeline.getText().length() > 0);
            assertTrue(checkDatePattern(timeline.getUser().getCreatedAt()));
        });
    }

    void checkTheNewStatus(UserStatus userStatus, String postedText) {
        assertNotNull(userStatus.getText());
        assertEquals(userStatus.getText(), postedText);
    }

    void checkTheErrorResponseCode(int actualStatusCode, int expectedStatusCode) {
        assertTrue(actualStatusCode == expectedStatusCode);
    }

    void checkTheDestroyedStatus(UserStatus destoryedStatus, String textOfAddedStatus, long newStatusId) {
        assertTrue(destoryedStatus.getId() == newStatusId);
        assertEquals(destoryedStatus.getText(), textOfAddedStatus);
    }
}
