package seleniumTests;

import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import selenium.utils.SetupPages;
import selenium.utils.pageObjects.AuthorizationForm;
import selenium.utils.pageObjects.MainPage;
import selenium.utils.pageObjects.NewStatusPage;

import static org.junit.Assert.*;

public class TimelineTest extends SetupPages {

    @Test
    public void getHomeTimelinesTest() {
        WebDriver driver = new FirefoxDriver(setUp());
        driver.get(TWITTER_HOME);
        AuthorizationForm authorizationForm = new AuthorizationForm(driver);
        MainPage mainPage = authorizationForm.authorize(LOGIN, PASSWORD);
        mainPage.getListOfPopularTwitts().forEach(userStatus -> {
            assertNotNull(userStatus.getText());
            assertNotNull(userStatus.getCreatedAt());
            assertTrue(userStatus.getText().length() >= 0);
            assertTrue(userStatus.getRetweetCount() == null || userStatus.getRetweetCount() > 0);
        });
        driver.quit();
    }

    @Test
    public void updateStatus(){
        WebDriver driver = new FirefoxDriver(setUp());
        driver.get(TWITTER_HOME);
        AuthorizationForm authorizationForm = new AuthorizationForm(driver);
        MainPage mainPage = authorizationForm.authorize(LOGIN, PASSWORD);
        NewStatusPage postPage = mainPage.postNewStatus();
        String twittText = getTimestamp() + " second to Mars.";
        mainPage = postPage.sendTwittTextAndPostTheTwitt(twittText);
        assertEquals(mainPage.getTheFirstStatus().getText(), twittText);
        driver.quit();
    }

    @Test
    public void destroyTheStatus(){
        WebDriver driver = new FirefoxDriver(setUp());
        driver.get(TWITTER_HOME);
        AuthorizationForm authorizationForm = new AuthorizationForm(driver);
        MainPage mainPage = authorizationForm.authorize(LOGIN, PASSWORD);
        NewStatusPage postPage = mainPage.postNewStatus();
        String twittText = getTimestamp() + " second to Mars.";
        mainPage = postPage.sendTwittTextAndPostTheTwitt(twittText);
        assertNotEquals(mainPage.destroyTheStatus().getText(), twittText);
        driver.quit();
    }
}
